object CaesarCipher
{
    def main(args: Array[String])
    {
        solve("HAL", 59);
    }

    def solve(string : String, maxShift : Int)
    {
        var shift = maxShift;

        if(maxShift < 1)
        {
            shift = 1;
        }
        for(i <- 1 to shift)
        {
            println("Caesar " + i + ": " + decrypt(string, i));
        }
    }

    def decrypt(string : String, shift : Int): String = 
    {
        return stringShift(string, shift);
    }
    
    def encrypt(string: String, shift : Int): String =
    {
        return stringShift(string, -shift);
    }

    def stringShift(string : String, offset : Int): String =
    {
        var newString : String = "";
        
        for(i <- 0 to string.length - 1)
        {
            newString += (((string.charAt(i).toInt + offset - 65) % 26) + 65).toChar;
        }

        return newString;
    }
}
