import Data.Char (ord, chr, isUpper, isLower)

main = do
    putStrLn (solve "HAL" 26)

solve :: [Char] -> Int -> [Char]
solve string 0      = ""
solve string shift  = "Caesar " ++ (show shift) ++ ": " ++ (decrypt string shift) ++ "\n" ++ (solve string (shift - 1)) 

decrypt :: [Char] -> Int -> [Char]
decrypt string shift = shiftString string shift

encrypt :: [Char] -> Int -> [Char]
encrypt string shift = shiftString string (-shift)

shiftString :: [Char] -> Int -> [Char]
shiftString string shift = map (shiftLetter shift) string

shiftLetter :: Int -> Char -> Char
shiftLetter shift letter
    | isUpper letter    = f 65
    | isLower letter    = f 97
    | otherwise         = letter
        where f offset = chr (((((ord letter) + shift) - offset) `mod` 26) + offset)
