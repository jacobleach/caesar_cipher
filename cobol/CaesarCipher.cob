       IDENTIFICATION DIVISION.
       PROGRAM-ID. CAESAR-CIPHER.
       AUTHOR.     JACOB LEACH.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 userString PIC A(100).
       01 shiftAmount PIC S99.
       PROCEDURE DIVISION.
           MOVE 26 TO shiftAmount
           MOVE "HAL" to userString
       
           CALL "SOLVE" USING BY CONTENT userString, shiftAmount
           EXIT PROGRAM.

       IDENTIFICATION DIVISION.
       PROGRAM-ID. SOLVE.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 loopIndex PIC 99.
       LINKAGE SECTION.
       01 inString Pic A(100).
       01 shift    Pic S99.
       PROCEDURE DIVISION USING inString, shift.
          PERFORM VARYING loopIndex FROM 0 BY 1 UNTIL loopIndex >
              shift
              DISPLAY "Caesar ", loopIndex, ": " WITH NO ADVANCING
              CALL "SHIFT" USING BY CONTENT inString, loopIndex 
          END-PERFORM.
          EXIT PROGRAM.
       END PROGRAM SOLVE.
       
       IDENTIFICATION DIVISION.
       PROGRAM-ID. ENCRYPT.
       DATA DIVISION.
       LINKAGE SECTION.
       01 inString Pic A(100).
       01 shift    Pic S99.
       PROCEDURE DIVISION USING inString, shift.
           MULTIPLY -1 BY shift
           CALL "SHIFT" USING BY CONTENT inString, shift
           EXIT PROGRAM.
       END PROGRAM ENCRYPT.
          
       IDENTIFICATION DIVISION.
       PROGRAM-ID. DECRYPT.
       DATA DIVISION.
       LINKAGE SECTION.
       01 inString Pic A(100).
       01 shift    Pic S99.
       PROCEDURE DIVISION USING inString, shift.
          CALL "SHIFT" USING BY CONTENT inString, shift
          EXIT PROGRAM.
       END PROGRAM DECRYPT.
       
       IDENTIFICATION DIVISION.
       PROGRAM-ID. SHIFT.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 CharValue  PIC 999.
       01 IndexValue PIC 999.
       01 newString  PIC A(100).
       LINKAGE SECTION.
       01 inString Pic A(100).
       01 shift    Pic S99.
       PROCEDURE DIVISION USING inString, shift.
          PERFORM VARYING IndexValue FROM 1 BY 1 UNTIL IndexValue >
               FUNCTION length(inString)
      *> Get the char value
               MOVE function ORD (inString(IndexValue:1)) TO CharValue
      *> If it isn't an upper case number, put a space in the string
               IF CharValue > 96 OR CharValue < 66 THEN
                   MOVE ' ' TO newString(IndexValue:1)
               ELSE
               ADD shift TO CharValue
               ADD -66 TO CharValue
               MOVE function MOD(CharValue, 26) TO CharValue
               ADD 66 TO CharValue
               MOVE function CHAR(CharValue) TO newString(IndexValue:1)
               END-IF
           END-PERFORM.
           DISPLAY newString
           EXIT PROGRAM.
       END PROGRAM SHIFT.
          
       END PROGRAM CAESAR-CIPHER.
