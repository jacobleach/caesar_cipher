object CaesarCipher
{
    def main(args: Array[String])
    {
        println(solve("HAL", 59));
    }

    def solve(string : String, shift : Int): String =
    {
        if(shift < 1) {
            return "";
        }
        return "Caesar " + shift + ": " + decrypt(string, shift) + "\n" + solve(string, shift - 1);
    }

    def decrypt(string : String, shift : Int): String = 
    {
        return stringShift(string, shift);
    }
    
    def encrypt(string: String, shift : Int): String =
    {
        return stringShift(string, -shift);
    }

    def stringShift(string : String, shift : Int): String =
    {
        return string map {shiftLetter(shift , _)};
    }

    def shiftLetter(shift : Int, letter : Char): Char =
    {
        if(letter.isUpper)
        {
            f(65, shift, letter);
        }
        else if(letter.isLower)
        {
            f(97, shift, letter);
        }
        else
        {
            return letter;
        }
    }

    def f(offset : Int, shift : Int, letter : Char): Char =
    {
        return ((((letter.toInt + shift) - offset) % 26) + offset).toChar;
    }
}
