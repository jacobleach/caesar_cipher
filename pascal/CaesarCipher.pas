program CaesarCipher;

function stringShift(myString : String; shift : Integer) : String;
var
    newString : String;
    charAsInt : Integer;
    i : Integer;
begin
    newString := '';
    for i := 1 to byte(myString[0]) do
        begin
            charAsInt := ord(myString[i]);
            newString := newString + chr(((charAsInt + shift - 65) mod 26) + 65);
        end;

    stringShift := newString;
end;

function decrypt(myString : String; shift : Integer) : String;
begin
    decrypt := stringShift(myString, shift);
end;

function encrypt(myString : String; shift : Integer) : String;
begin
    encrypt := stringShift(myString, -shift);
end;


procedure solve(myString : String; maxShift : Integer);
var
    shift : Integer;
    i : Integer;
    back : String;
begin
    shift := maxShift;

    if maxShift < 1 then
        shift := 1;
    
    for i := 0 to shift do
        begin
            back := decrypt(myString, i);
            Writeln('Caesar ', i, ' :', back);
        end
end;

begin
    solve('HAL', 26);
end.

