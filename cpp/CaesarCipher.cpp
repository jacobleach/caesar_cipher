#include <cstdlib>
#include <cctype>
#include <sstream>
#include <iostream>

using namespace std;

char doShiftLetter(const int shift, const char letter, const int offset) 
{
    return (((letter + shift) - offset) % 26) + offset;
}   

char shiftLetter(const int shift, const char letter) 
{
    if(isupper(letter)) 
    {
        return doShiftLetter(shift, letter, 65);
    }
    else if(islower(letter)) 
    {   
        return doShiftLetter(shift, letter, 97);
    }
    else 
    {
        return letter;
    }
}

string shiftString(const int shift, const string message)
{
    string temp;
    temp.resize(message.length());

    for(int i = 0; i < message.length(); i++)
    {
        temp[i] = shiftLetter(shift, message[i]);
    }

    return temp;
}

int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        cout << "Invalid number of args." << endl;
    }
    else
    {
        istringstream ss(argv[1]);
        int shift;
        shift = atoi(argv[1]);
        cout << shiftString(shift, argv[2]) << endl;
    }
}


