(defun main (message offset)
  (print (solve message offset)))

(defun solve (message offset)
  (if (< offset 1)
    ""
    (concatenate 'string "Caesar " (write-to-string offset) ": " (decrypt message offset) "
" (solve message (- offset 1)))))

(defun encrypt (message offset)
  (shiftMessage message (- offset)))

(defun decrypt (message offset)
  (shiftMessage message offset))

(defun shiftMessage (message offset)
  (map 'string #' (lambda (c) (shiftLetter c offset)) message))

(defun shiftLetter (letter offset)
  (code-char (+ (mod (- (+ (char-code letter) offset) 65) 26) 65)))
