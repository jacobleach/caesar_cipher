-module(cipher).
-export([main/0]).

main() -> io:format(solve("HAL", 26)).

solve(_, Shift) 
    when Shift == 0 -> "";
solve(Message, Shift) 
    -> "Caesar " ++ integer_to_list(Shift) ++ ": " ++ decrypt(Message, Shift) ++ "\n" ++ solve(Message, (Shift - 1)).

decrypt(Message, Shift) -> shift_message(Message, Shift).

encrypt(Message, Shift) -> shift_message(Message, -Shift).

shift_message(Message, Shift) -> lists:map(fun(Char) -> shift_letter(Char, Shift) end, Message).

shift_letter(Letter, Shift) -> (((Letter + Shift) - 65) rem 26) + 65.
