function solve(string, maxShift) {
    var shift = maxShift;
    
    if(maxShift < 1) {
        shift = 1;
    } 
    else {
        for(var i = 0; i < shift; i++) {
            console.log("Caesar " + i + ": " + decrypt(string, i));
        }
    }
}

function encrypt(string, offset) {
    return stringShift(string, -offset);
}

function decrypt(string, offset) {
    return stringShift(string, offset);
}

var stringShift = function(string, offset) {
    var newString = "";
    
    for(var i = 0; i < string.length; i++) {
        newString += String.fromCharCode(mod(((string.charCodeAt(i) + offset) - 65),  26) + 65);
        console.log(string.charCodeAt(i) + offset - 65);
    }

    return newString;
};

var mod = function (n, m) {
    var remain = n % m;
    return Math.floor(remain >= 0 ? remain : remain + m);
};
